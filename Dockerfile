FROM amazoncorretto:17
COPY build/docker/main/layers/libs /home/app/libs
COPY build/docker/main/layers/classes /home/app/classes
COPY build/docker/main/layers/resources /home/app/resources
COPY build/docker/main/layers/application.jar /home/app/application.jar
WORKDIR /home/app
EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/home/app/application.jar"]